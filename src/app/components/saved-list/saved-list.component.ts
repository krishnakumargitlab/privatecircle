import {
  Component,
  OnInit,
  OnChanges,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
} from '@angular/core';
import { ListnameService } from 'src/app/services/listname.service';
import { ListName } from 'src/app/interfaces/listname';

@Component({
  selector: 'app-saved-list',
  templateUrl: './saved-list.component.html',
  styleUrls: ['./saved-list.component.css'],
})
export class SavedListComponent implements OnInit, OnChanges {
  @Input() query: string = '';
  @Output() listIdOut: EventEmitter<number> = new EventEmitter<number>();

  listnames: ListName[] = [];
  listForSearch: ListName[] = [];
  activeListId: number = 0;
  constructor(private listNameService: ListnameService) {}

  ngOnInit(): void {
    this.getListNames();
  }
  // filter list based on query
  ngOnChanges(changes: SimpleChanges): void {
    var searchTerm = changes['query'].currentValue;
    if (!searchTerm) {
      this.listnames = [...this.listForSearch];
    } else {
      this.listnames = this.listForSearch.filter((item) => {
        let filteredItem = item.listname.toLowerCase().includes(searchTerm);
        return filteredItem;
      });
    }
  }
  // fetch list from service
  getListNames(): void {
    this.listnames = this.listNameService.getListNames();
    this.listForSearch = [...this.listnames];
  }
  // emit id on detail button click
  fetchListDetail(id: number) {
    // fire event
    this.listIdOut.emit(id);
    this.activeListId = id;
  }
}
