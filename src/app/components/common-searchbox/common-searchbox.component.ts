import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormsModule } from '@angular/forms';
@Component({
  selector: 'app-common-searchbox',
  templateUrl: './common-searchbox.component.html',
  styleUrls: ['./common-searchbox.component.css'],
})
export class CommonSearchboxComponent implements OnInit {
  @Output() queryOut: EventEmitter<string> = new EventEmitter<string>();
  query: string = '';
  constructor() {}

  ngOnInit(): void {}
  //emit query string entered in input box
  sendQuery() {
    this.queryOut.emit(this.query.trim());
  }
}
