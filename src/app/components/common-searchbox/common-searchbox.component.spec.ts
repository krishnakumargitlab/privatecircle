import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonSearchboxComponent } from './common-searchbox.component';

describe('CommonSearchboxComponent', () => {
  let component: CommonSearchboxComponent;
  let fixture: ComponentFixture<CommonSearchboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommonSearchboxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonSearchboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
