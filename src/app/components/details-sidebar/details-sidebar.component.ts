import {
  Component,
  OnInit,
  OnChanges,
  Input,
  SimpleChanges,
} from '@angular/core';
import { ListnameService } from 'src/app/services/listname.service';

@Component({
  selector: 'app-details-sidebar',
  templateUrl: './details-sidebar.component.html',
  styleUrls: ['./details-sidebar.component.css'],
})
export class DetailsSidebarComponent implements OnInit, OnChanges {
  @Input() listId: number = 0;
  Details: string[] = [];
  constructor(private listService: ListnameService) {}

  ngOnInit(): void {}
  // update list detail on @input change
  ngOnChanges(changes: SimpleChanges): void {
    let id = changes['listId'].currentValue;
    let response = this.listService.getListDetails(id);
    this.Details = response[0].detail;
  }
}
