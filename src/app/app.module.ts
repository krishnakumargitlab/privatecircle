import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { CommonSearchboxComponent } from './components/common-searchbox/common-searchbox.component';
import { SavedListComponent } from './components/saved-list/saved-list.component';
import { DetailsSidebarComponent } from './components/details-sidebar/details-sidebar.component';

@NgModule({
  declarations: [
    AppComponent,
    CommonSearchboxComponent,
    SavedListComponent,
    DetailsSidebarComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
