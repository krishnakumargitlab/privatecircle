import { ListName } from './../interfaces/listname';

export const ListNames: ListName[] = [
  { id: 1, date: 'Mar 10', listname: 'kdxkdddd', entitycount: 0 },
  { id: 2, date: 'Feb 28', listname: 'dcfvgbh', entitycount: 0 },
  { id: 3, date: 'Jun 13', listname: 'vtbhbhh', entitycount: 0 },
  {
    id: 4,
    date: 'Nov 01',
    listname: 'ndwencjrenjrenknekmdkw edjen njfnreife 3firf',
    entitycount: 12,
  },
  { id: 5, date: 'Mar 24', listname: 'dnvnijeiid edneinfi', entitycount: 23 },
  {
    id: 6,
    date: 'Apr 12',
    listname: 'dewjnewcnewi iejie freirer',
    entitycount: 21,
  },
  {
    id: 7,
    date: 'May 07',
    listname: 'funfurfn freijf rfnijfr refijri',
    entitycount: 11,
  },
  { id: 8, date: 'Jul 31', listname: 'feniwft', entitycount: 10 },
  { id: 9, date: 'Oct 11', listname: 'fnieidrfirt', entitycount: 21 },
  { id: 10, date: 'Nov 20', listname: 'fenivtrvjtn', entitycount: 18 },
  { id: 11, date: 'Aug 12', listname: 'frniefiregrfrgt', entitycount: 11 },
  { id: 12, date: 'Jan 03', listname: 'fnindief', entitycount: 17 },
  { id: 13, date: 'Apr 29', listname: 'diwdijfvejn rfrifr', entitycount: 5 },
  {
    id: 14,
    date: 'Feb 16',
    listname: 'firefir rtgyhiejd thjt54',
    entitycount: 20,
  },
  {
    id: 15,
    date: 'Aug 10',
    listname: 'fnrifr fnirf rfirjgitg frjftgjt',
    entitycount: 19,
  },
];
