import { ListDetail } from './../interfaces/listDetail';

export const ListDetails: ListDetail[] = [
  {
    id: 1,
    detail: [
      'BASETECH FACTORY PRIVATE LIMITED',
      'MINDTWEEN INFOTECH PRIVATE LIMITED',
      'MAGUZLABS TECHNOLOGY SERVICES PRIVATE LIMITED',
      'GREENIYA TECH SOLUTIONS PRIVATE LIMITED',
      'INDOLUTIONS TECHNOLOGIES PRIVATE LIMITED',
      'UROGULF TECHNOLOGIES PRIVATE LIMITED',
      'AL RAZA SOLUTIONS PRIVATE LIMITED',
      'XTAL LABZ PRIVATE LIMITED',
      'SPAAR INFOTECH (INDIA) PRIVATE LIMITED',
      'EXELIXIR ANALYTICS PRIVATE LIMITED',
      'AUTRAM INFOTECH PRIVATE LIMITED',
      'NFONICS SOLUTIONS PRIVATE LIMITED',
      'ZAINA SOFT SOLUTIONS PRIVATE LIMITED',
      'ACCURATE COMPUTING SOLUTIONS PRIVATE LIMITED',
      'IRC TECHNOLOGIES PRIVATE LIMITED',
      'V-LABS TECHNOLOGIES PRIVATE LIMITED',
      'UNCANNY VISION SOLUTIONS PRIVATE LIMITED',
      'NAWRUS SYSTEMS (INDIA) PRIVATE LIMITED',
    ],
  },
  {
    id: 2,
    detail: ['row1-id2', 'row2-id2', 'row3-id2', 'row4-id2', 'row5-id2'],
  },
  {
    id: 3,
    detail: ['row1-id3', 'row2-id3', 'row3-id3', 'row4-id3', 'row5-id3'],
  },
  {
    id: 4,
    detail: ['row1-id4', 'row2-id4', 'row3-id4', 'row4-id4', 'row5-id4'],
  },
  {
    id: 5,
    detail: ['row1-id5', 'row2-id5', 'row3-id5', 'row4-id5', 'row5-id5'],
  },
  {
    id: 6,
    detail: ['row1-id6', 'row2-id6', 'row3-id6', 'row4-id6', 'row5-id6'],
  },
  {
    id: 7,
    detail: ['row1-id7', 'row2-id7', 'row3-id7', 'row4-id7', 'row5-id7'],
  },
  {
    id: 8,
    detail: ['row1-id8', 'row2-id8', 'row3-id8', 'row4-id8', 'row5-id8'],
  },
  {
    id: 9,
    detail: ['row1-id9', 'row2-id9', 'row3-id9', 'row4-id9', 'row5-id9'],
  },
  {
    id: 10,
    detail: ['row1-id10', 'row2-id10', 'row3-id10', 'row4-id10', 'row5-id10'],
  },
  {
    id: 11,
    detail: ['row1-id11', 'row2-id11', 'row3-id11', 'row4-id11', 'row5-id11'],
  },
  {
    id: 12,
    detail: ['row1-id12', 'row2-id12', 'row3-id12', 'row4-id12', 'row5-id12'],
  },
  {
    id: 13,
    detail: ['row1-id13', 'row2-id13', 'row3-id13', 'row4-id13', 'row5-id13'],
  },
  {
    id: 14,
    detail: ['row1-id14', 'row2-id14', 'row3-id14', 'row4-id14', 'row5-id14'],
  },
  {
    id: 15,
    detail: ['row1-id15', 'row2-id15', 'row3-id15', 'row4-id15', 'row5-id15'],
  },
];
