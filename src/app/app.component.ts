import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  queryString: string = '';
  listIdForDetails: number = 0;
  title = 'Private Circle';
  // populate query
  sendQuery(query: string) {
    this.queryString = query;
  }
  // populate id
  sendListId(id: number) {
    this.listIdForDetails = id;
  }
}
