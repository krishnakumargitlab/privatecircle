import { Injectable } from '@angular/core';
import { ListName } from './../interfaces/listname';
import { ListNames } from './../data/dummy-listnames';
import { ListDetail } from './../interfaces/listDetail';
import { ListDetails } from './../data/dummy-listDetails';

@Injectable({
  providedIn: 'root',
})
export class ListnameService {
  // send list name
  getListNames(): ListName[] {
    return ListNames;
  }
  // send list detail of particular id
  getListDetails(id: number): ListDetail[] {
    let response = ListDetails.filter((item) => item.id == id);
    return response;
  }
}
