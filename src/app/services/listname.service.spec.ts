import { TestBed } from '@angular/core/testing';

import { ListnameService } from './listname.service';

describe('ListnameService', () => {
  let service: ListnameService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListnameService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
