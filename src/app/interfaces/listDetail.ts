export interface ListDetail {
  id: number;
  detail: string[];
}
