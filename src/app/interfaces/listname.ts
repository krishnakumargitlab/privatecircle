export interface ListName {
  id: number;
  date: string;
  listname: string;
  entitycount: number;
}
